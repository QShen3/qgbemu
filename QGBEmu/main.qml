import QtQml
import QtQuick.Controls
import Qt.labs.platform as Labs
import Qt.labs.settings

import QGBEmu

ApplicationWindow {
    id: app;
    visible: true;
    title: qsTr("QGBEmu");
    x: windowSettings.position.x;
    y: windowSettings.position.y;
    width: windowSettings.windowSize.width;
    height: windowSettings.windowSize.height + menuBar.height;

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File");
            Action {
                text: qsTr("&Open...");
                onTriggered: fileDialog.open();
            }
            MenuSeparator {}
            Action {
                text: qsTr("&Quit");
                onTriggered: Qt.quit();
            }
        }
    }

    Screen {
        id: screen;
        anchors.fill: parent;
        getOutputFunction: gpu.getGetOutputFunction();
        outputSyncFunction: gpu.outputSyncFunction();
        Connections {
            target: gpu;
            function onVBlankEntered() {
                screen.update();
            }
        }
    }

    Labs.FileDialog {
        id: fileDialog;
        onAccepted: qgbemu.loadCartridge(fileDialog.file);
    }

    Settings {
        id: windowSettings;
        category: "Window";

        property int multiplier: 2;
        property point position: Qt.point(200, 200);
        property size windowSize: Qt.size(160 * multiplier, 144 * multiplier);
        property bool autoFitOutput: true;
    }

    Component.onDestruction: {
        windowSettings.position = Qt.point(app.x, app.y);
    }
}
