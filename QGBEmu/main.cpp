/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <iostream>

#include <QtQml/QQmlApplicationEngine>
#include <QtWidgets/QApplication>

#include "QGBEmu/QGBEmu.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QApplication::setOrganizationName("QShen");
    QApplication::setOrganizationDomain("blog.qshen.cc");
    QApplication::setApplicationName("QGBEmu");

    QQmlApplicationEngine engine;
    qgbemu::QGBEmu qgbEmu(engine);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
