/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <atomic>
#include <cstddef>
#include <cstdint>
#include <array>
#include <mutex>
#include <functional>
#include <stack>

#include <opencv2/core/mat.hpp>
#include <QtCore/QObject>
#include <QtCore/QSettings>

#include "QGBEmu/Cartridge/GbCartridge.h"
#include "QGBEmu/Gpu/GpuInterface.h"
#include "QGBEmu/Memory/Ram.h"

namespace qgbemu
{

constexpr size_t videoRamBankSize = 0x2000;
constexpr size_t videoRamBankCount = 2;
constexpr size_t spriteAttributeTableSize = 0xA0;

class GbGpu : public QObject, public GpuInterface<>
{
    Q_OBJECT
    public:
        using Tile = std::array<std::array<uint8_t, 8>, 8>;

        GbGpu() = delete;
        explicit GbGpu(const GbCartridge& gbCartridge);
        ~GbGpu() = default;

        const cv::Mat& getOutput() const override;
        std::unique_lock<std::mutex> outputSync() override;

        void start() override;
        void stop() override;
        void exec() override;

        uint8_t readFromVideoRam(size_t address) const override;
        void writeToVideoRam(size_t address, const uint8_t& value) override;

        uint8_t readBackgroundColorPaletteData() const;
        void writeBackgroundColorPaletteData(uint8_t value);
        uint8_t readSpriteColorPaletteData() const;
        void writeSpriteColorPaletteData(uint8_t value);

        uint8_t readFromSpriteAttributeTable(size_t address) const;
        void writeToSpriteAttributeTable(size_t address, uint8_t value);

        struct {
            union {
                struct {
                    uint backgroundAndWindowsPriority : 1;
                    uint spriteEnable : 1;
                    uint spriteSize : 1;
                    uint backgroundTileMapArea : 1;
                    uint backgroundAndWindowTileDataArea : 1;
                    uint windowEnable : 1;
                    uint windowTileMapArea : 1;
                    uint lcdEnable : 1;
                };
                uint8_t lcdControl;
            };  // FF40
            union {
                struct {
                    uint modeFlag : 2;
                    uint coincidenceFlag : 1;
                    uint mode0HBlankInterrupt : 1;
                    uint mode1VBlankInterrupt : 1;
                    uint mode2OAMInterrupt : 1;
                    uint lyCoincidenceInterrupt : 1;
                };
                uint8_t lcdStatus;
            };  // FF41

            uint8_t scrollY;  // FF42
            uint8_t scrollX;  // FF43
            uint8_t lcdYCoordinate;  // FF44
            uint8_t lcdYCoordinateCompare;  // FF45

            uint8_t dMATransferAndStartAddress;  // FF46

            union {
                struct {
                    uint shadeForBackgroundColorNumber0 : 2;
                    uint shadeForBackgroundColorNumber1 : 2;
                    uint shadeForBackgroundColorNumber2 : 2;
                    uint shadeForBackgroundColorNumber3 : 2;
                };
                uint8_t backgroundPaletteData;
            };  // FF47
            union {
                struct {
                    uint shadeForSprite0ColorNumber0 : 2;
                    uint shadeForSprite0ColorNumber1 : 2;
                    uint shadeForSprite0ColorNumber2 : 2;
                    uint shadeForSprite0ColorNumber3 : 2;
                };
                uint8_t spritePalette0Data;
            };  // FF48
            union {
                struct {
                    uint shadeForSprite1ColorNumber0 : 2;
                    uint shadeForSprite1ColorNumber1 : 2;
                    uint shadeForSprite1ColorNumber2 : 2;
                    uint shadeForSprite1ColorNumber3 : 2;
                };
                uint8_t spritePalette1Data;
            };  // FF49

            uint8_t windowYPosition;  // FF4A
            uint8_t windowXPosition;  // FF4B

            uint8_t videoRamBank;  // FF4F

            union {
                struct {
                    uint backgroundPaletteIndex : 6;
                    uint : 1;
                    uint backgroundAutoIncrement : 1;
                };
                uint8_t backgroundColorPaletteSpecification;
            };  // FF68
            uint8_t backgroundColorPaletteData;  // FF69
            union {
                struct {
                    uint spritePaletteIndex : 6;
                    uint : 1;
                    uint spriteAutoIncrement : 1;
                };
                uint8_t spriteColorPaletteSpecification;
            };  // FF6A
            uint8_t spriteColorPaletteData;  // FF6B

            uint8_t newDMASourceHigh;  // FF51
            uint8_t newDMASourceLow;  // FF52
            uint8_t newDMADestinationHigh;  // FF53
            uint8_t newDMADestinationLow;  // FF54
            uint8_t newDMALength;  // FF55

            
        } registers;

    public slots:
        std::function<const cv::Mat&()> getGetOutputFunction() const;
        std::function<std::unique_lock<std::mutex>()> outputSyncFunction();

    signals:
        void vBlankEntered();
        void statTriggered();

    private:
        union GbcColor {
            struct {
                uint red : 5;
                uint green : 5;
                uint blue : 5;
                uint : 1;
            };
            uint16_t color;
        };
        union GbcTileAttribute {
            struct {
                uint backgroundPaletteNumber : 3;
                uint tileVideoRamBank : 1;
                uint : 1;
                uint horizontalFlip : 1;
                uint verticalFlip : 1;
                uint backgroundToOAMPriority : 1;
            };
            uint8_t attribute;
        };
        union GbcSpriteAttribute {
            struct {
                uint gbcPaletteNumber : 3;
                uint tileVideoRamBank : 1;
                uint paletteNumber : 1;
                uint horizontalFlip : 1;
                uint verticalFlip : 1;
                uint backgroundToOAMPriority : 1;
            };
            uint8_t attribute;
        };

        void checklcdYCoordinate();
        void scanSprites();
        void renderLine();
        uint16_t backgroundOrWindowColor(uint8_t x, uint8_t y, bool isBackground);
        cv::Vec3b getColor(uint16_t index) const;

        std::atomic_bool started;
        uint16_t ticks;
        uint16_t dmaTicks;
        const GbCartridge& gbCartridge;

        std::mutex outputDataMutex;
        cv::Mat outputData;
        std::array<Ram<uint8_t, videoRamBankSize>, videoRamBankCount> videoRamBanks;
        Ram<uint8_t, spriteAttributeTableSize> spriteAttributeTable;
        Ram<uint8_t, 0x3F> gbcBackgroundPalettesData;
        Ram<uint8_t, 0x3F> gbcSpritePalettesData;

        std::stack<uint8_t> spriteStack;
        std::array<std::array<bool, 144>, 160> backgroundToOAMPriorityMap;
        std::array<std::array<uint8_t, 144>, 160> colorIndexMap;
        uint16_t windowLineCounter;

        QSettings settings;
};

}  // namespace qgbemu
