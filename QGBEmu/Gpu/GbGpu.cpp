/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cstddef>
#include <cstdint>
#include <cstring>
#include <functional>
#include <mutex>
#include <stack>

#include <opencv2/core/mat.hpp>

#include "QGBEmu/Gpu/GbGpu.h"

namespace qgbemu
{

GbGpu::GbGpu(const GbCartridge& gbCartridge) :
    started(false),
    ticks(0),
    dmaTicks(0),
    gbCartridge(gbCartridge),
    outputData(144, 160, CV_8UC3),
    windowLineCounter(0)
{
}

const cv::Mat& GbGpu::getOutput() const
{
    return outputData;
}

std::unique_lock<std::mutex> GbGpu::outputSync()
{
    std::unique_lock<std::mutex> outputDataLock(outputDataMutex);
    return outputDataLock;
}

void GbGpu::start()
{
    if (!started.load()) {
        memset(&registers, 0, sizeof(registers));
        registers.lcdControl = 0x91;
        registers.lcdStatus = 0x85;
        registers.dMATransferAndStartAddress = 0xFF;
        registers.backgroundPaletteData = 0xFC;
        registers.videoRamBank = 0xFF;
        registers.backgroundColorPaletteSpecification = 0xFF;
        registers.backgroundColorPaletteData = 0xFF;
        registers.spriteColorPaletteSpecification = 0xFF;
        registers.spriteColorPaletteData = 0xFF;

        ticks = 0;
        started.store(true);
    }
}

void GbGpu::stop()
{
    started.store(false);
}

void GbGpu::exec()
{
    if (!started.load()) {
        return;
    }
    if (registers.lcdEnable == 0) {
        ticks = 0;
        return;
    }
    if (ticks == 0) {
        if (registers.modeFlag == 0) {
            if (registers.lcdYCoordinate == 143) {
                registers.modeFlag = 1;
                ticks += 456;
                emit vBlankEntered();
                if (registers.mode1VBlankInterrupt) {
                    emit statTriggered();
                }
            }
            else {
                registers.modeFlag = 2;
                ticks += 80;
                if (registers.mode2OAMInterrupt) {
                    emit statTriggered();
                }
            }
            registers.lcdYCoordinate++;
            checklcdYCoordinate();
        }
        else if (registers.modeFlag == 1) {
            if (registers.lcdYCoordinate == 153) {
                registers.lcdYCoordinate = 0;
                windowLineCounter = 0;
                registers.modeFlag = 2;
                ticks += 80;
            }
            else {
                ticks+= 456;
                registers.lcdYCoordinate++;
            }
            checklcdYCoordinate();
        }
        else if (registers.modeFlag == 2) {
            scanSprites();
            registers.modeFlag = 3;
            ticks += 172;
        }
        else if (registers.modeFlag == 3) {
            std::unique_lock<std::mutex> outputDataLock(outputDataMutex);
            renderLine();
            outputDataLock.unlock();
            registers.modeFlag = 0;
            ticks += 204;
            if (registers.mode0HBlankInterrupt) {
                emit statTriggered();
            }
        }
    }
    else {
        ticks--;
    }
}

uint8_t GbGpu::readFromVideoRam(size_t address) const
{
    if (registers.modeFlag == 3 && registers.lcdEnable == 1) {
        return 0xFF;
    }
    if (gbCartridge.isGbcMode()) {
        return videoRamBanks.at(registers.videoRamBank).read(address);
    }
    return videoRamBanks.at(0).read(address);
}

void GbGpu::writeToVideoRam(size_t address, const uint8_t& value)
{
    if (registers.modeFlag == 3 && registers.lcdEnable == 1) {
        return;
    }
    if (gbCartridge.isGbcMode()) {
        videoRamBanks.at(registers.videoRamBank).write(address, value);
    }
    else {
        videoRamBanks.at(0).write(address, value);
    }
}

uint8_t GbGpu::readBackgroundColorPaletteData() const
{
    return gbcBackgroundPalettesData.read(registers.backgroundPaletteIndex);
}

void GbGpu::writeBackgroundColorPaletteData(uint8_t value)
{
    registers.backgroundColorPaletteData = value;
    gbcBackgroundPalettesData.write(registers.backgroundPaletteIndex, value);
    if(registers.backgroundAutoIncrement) {
        registers.backgroundPaletteIndex++;
    }
}

uint8_t GbGpu::readSpriteColorPaletteData() const
{
    return gbcSpritePalettesData.read(registers.spritePaletteIndex);
}

void GbGpu::writeSpriteColorPaletteData(uint8_t value)
{
    registers.spriteColorPaletteData = value;
    gbcSpritePalettesData.write(registers.spritePaletteIndex, value);
    if(registers.spriteAutoIncrement) {
        registers.spritePaletteIndex++;
    }
}

uint8_t GbGpu::readFromSpriteAttributeTable(size_t address) const
{
    if ((registers.modeFlag == 2 || registers.modeFlag == 3) && registers.lcdEnable == 1) {
        return 0xFF;
    }
    return spriteAttributeTable.read(address);
}

void GbGpu::writeToSpriteAttributeTable(size_t address, uint8_t value)
{
    if ((registers.modeFlag == 2 || registers.modeFlag == 3) && registers.lcdEnable == 1) {
        return;
    }
    spriteAttributeTable.write(address, value);
}

std::function<const cv::Mat&()> GbGpu::getGetOutputFunction() const
{
    return std::bind(&GbGpu::getOutput, this);
}

std::function<std::unique_lock<std::mutex>()> GbGpu::outputSyncFunction()
{
    return std::bind(&GbGpu::outputSync, this);
}

void GbGpu::checklcdYCoordinate()
{
    if (registers.lcdYCoordinate == registers.lcdYCoordinateCompare) {
        registers.coincidenceFlag = 1;
        if (registers.lyCoincidenceInterrupt) {
            emit statTriggered();
        }
    }
    else {
        registers.coincidenceFlag = 0;
    }
}

void GbGpu::scanSprites()
{
    spriteStack = {};
    for (uint8_t i = 0; i < 160; i += 4) {
        int16_t y = spriteAttributeTable.read(i) - 16;
        if (registers.spriteSize == 1) {
            if (int16_t(registers.lcdYCoordinate) >= y && int16_t(registers.lcdYCoordinate) < (y + 16)) {
                spriteStack.push(i);
            }
        }
        else {
            if (int16_t(registers.lcdYCoordinate) >= y && int16_t(registers.lcdYCoordinate) < (y + 8)) {
                spriteStack.push(i);
            }
        }
        if (spriteStack.size() >= 10) {
            break;
        }
    }
}

void GbGpu::renderLine()
{
    if (registers.lcdEnable == 0) {
        return;
    }
    for (auto &data : backgroundToOAMPriorityMap) {
        std::fill_n(data.begin(), data.size(), false);
    }
    bool isWindowDrawn = false;
    for (uint8_t i = 0; i < 160; i++) {
        outputData.at<cv::Vec3b>(registers.lcdYCoordinate, i) = cv::Vec3b(255, 255, 255);
        // Draw background
        if (gbCartridge.isGbcMode() || registers.backgroundAndWindowsPriority == 1) {
            uint16_t colorNumber = backgroundOrWindowColor(i, registers.lcdYCoordinate, true);
            outputData.at<cv::Vec3b>(registers.lcdYCoordinate, i) = getColor(colorNumber);
        }

        // Draw window
        if ((gbCartridge.isGbcMode() || registers.backgroundAndWindowsPriority == 1) &&
                (registers.windowEnable == 1 && (registers.windowXPosition - 7) <= i && registers.windowYPosition <= registers.lcdYCoordinate)) {
            uint16_t colorNumber = backgroundOrWindowColor(i, registers.lcdYCoordinate, false);
            outputData.at<cv::Vec3b>(registers.lcdYCoordinate, i) = getColor(colorNumber);
            isWindowDrawn = true;
        }
    }
    if (isWindowDrawn) {
        windowLineCounter++;
    }
    if (registers.spriteEnable == 0) {
        return;
    }
    while (!spriteStack.empty())
    {
        uint8_t spriteIndex = spriteStack.top();
        spriteStack.pop();

        uint8_t tileIndex = spriteAttributeTable.read(spriteIndex + 2);
        if (registers.spriteSize == 1) {
            tileIndex &= 0xFE;
        }
        GbcSpriteAttribute spriteAttribute { .attribute = 0 };
        spriteAttribute.attribute = spriteAttributeTable.read(spriteIndex + 3);

        bool isBank1 = gbCartridge.isGbcMode() && (spriteAttribute.tileVideoRamBank == 1);
        auto& videoRamBank = isBank1 ? videoRamBanks[1] : videoRamBanks[0];

        for (uint8_t i = 0; i < 160; i++) {
            if (backgroundToOAMPriorityMap[i][registers.lcdYCoordinate] && colorIndexMap[i][registers.lcdYCoordinate] > 0) {
                if (gbCartridge.isGbcMode()) {
                    if (registers.backgroundAndWindowsPriority == 1) {
                        continue;
                    }
                }
                else {
                    continue;
                }
            }
            if (spriteAttribute.backgroundToOAMPriority == 1 && colorIndexMap[i][registers.lcdYCoordinate] > 0) {
                if (gbCartridge.isGbcMode()) {
                    if (registers.backgroundAndWindowsPriority == 1) {
                        continue;
                    }
                }
                else {
                    continue;
                }
            }
            int16_t pixelX = i - spriteAttributeTable.read(spriteIndex + 1) + 8;
            if (pixelX >= 8) {
                break;
            }
            if (pixelX < 0) {
                continue;
            }
            int16_t pixelY = registers.lcdYCoordinate - spriteAttributeTable.read(spriteIndex) + 16;
            uint8_t realTileIndex = tileIndex;
            if (registers.spriteSize == 1) {
                if (pixelY < 8) {
                    if (spriteAttribute.verticalFlip == 1) {
                        realTileIndex += 1;
                    }
                }
                else {
                    if (spriteAttribute.verticalFlip == 0) {
                        realTileIndex += 1;
                    }
                }
            }
            pixelY &= 0x7;
            size_t lineIndex = realTileIndex * 16 + (spriteAttribute.verticalFlip ? (7 - ((pixelY * 8 + pixelX) / 8)) : (pixelY * 8 + pixelX) / 8) * 2;
            uint8_t bitIndex = spriteAttribute.horizontalFlip ? 1 << pixelX : 1 << (7 - pixelX);
            uint8_t colorIndex = ((videoRamBank.read(lineIndex) & bitIndex) ? 1 : 0) + ((videoRamBank.read(lineIndex + 1) & bitIndex) ? 2 : 0);

            if (colorIndex == 0) {
                continue;
            }
            uint16_t colorNumber = 0;
            if (gbCartridge.isGbcMode()) {
                uint8_t gbcColorIndex = spriteAttribute.gbcPaletteNumber * 8 + colorIndex * 2;
                colorNumber = (uint16_t(gbcSpritePalettesData.read(gbcColorIndex + 1)) << 8) | gbcSpritePalettesData.read(gbcColorIndex);
            }
            else if (spriteAttribute.paletteNumber == 0) {
                switch (colorIndex) {
                case 0:
                    colorNumber = registers.shadeForSprite0ColorNumber0;
                    break;
                case 1:
                    colorNumber = registers.shadeForSprite0ColorNumber1;
                    break;
                case 2:
                    colorNumber = registers.shadeForSprite0ColorNumber2;
                    break;
                case 3:
                    colorNumber = registers.shadeForSprite0ColorNumber3;
                    break;
                default:
                    break;
                }
            }
            else {
                switch (colorIndex) {
                case 0:
                    colorNumber = registers.shadeForSprite1ColorNumber0;
                    break;
                case 1:
                    colorNumber = registers.shadeForSprite1ColorNumber1;
                    break;
                case 2:
                    colorNumber = registers.shadeForSprite1ColorNumber2;
                    break;
                case 3:
                    colorNumber = registers.shadeForSprite1ColorNumber3;
                    break;
                default:
                    break;
                }
            }
            outputData.at<cv::Vec3b>(registers.lcdYCoordinate, i) = getColor(colorNumber);
        }
    }
}

uint16_t GbGpu::backgroundOrWindowColor(uint8_t x, uint8_t y, bool isBackground)
{
    uint8_t mapX, mapY;
    if (isBackground) {
        mapX = (x + registers.scrollX) % 256;
        mapY = (y + registers.scrollY) % 256;
    }
    else {
        mapX = x - registers.windowXPosition + 7;
        mapY = windowLineCounter;
    }
    uint8_t tileX = mapX / 8, tileY = mapY / 8;
    GbcTileAttribute tileAttribute { .attribute = 0};
    size_t mapOffset = 0;
    if (isBackground) {
        mapOffset = registers.backgroundTileMapArea == 0 ? 0x1800 : 0x1c00;
    }
    else {
        mapOffset = registers.windowTileMapArea == 0 ? 0x1800 : 0x1c00;
    }
    uint8_t tileIndex = videoRamBanks[0].read(mapOffset + (tileY * 32 + tileX));
    if (gbCartridge.isGbcMode()) {
        tileAttribute.attribute = videoRamBanks[1].read(mapOffset + (tileY * 32 + tileX));
    }
    if (tileAttribute.backgroundToOAMPriority == 1) {
        backgroundToOAMPriorityMap[x][y] = true;
    }

    bool isBank1 = gbCartridge.isGbcMode() && (tileAttribute.tileVideoRamBank == 1);
    uint8_t pixelX = mapX % 8, pixelY = mapY % 8;
    uint8_t bitIndex = tileAttribute.horizontalFlip ? 1 << pixelX : 1 << (7 - pixelX);
    size_t lineIndex = tileIndex * 16 + (tileAttribute.verticalFlip ? (7 - ((pixelY * 8 + pixelX) / 8)) : (pixelY * 8 + pixelX) / 8) * 2;
    if (tileIndex < 128) {
        lineIndex += registers.backgroundAndWindowTileDataArea == 1 ? 0 : 0x1000;
    }
    auto& videoRamBank = isBank1 ? videoRamBanks[1] : videoRamBanks[0];
    uint8_t colorIndex = ((videoRamBank.read(lineIndex) & bitIndex) ? 1 : 0) + ((videoRamBank.read(lineIndex + 1) & bitIndex) ? 2 : 0);
    colorIndexMap[x][y] = colorIndex;

    uint16_t colorNumber = 0;
    if (gbCartridge.isGbcMode()) {
        uint8_t gbcColorIndex = tileAttribute.backgroundPaletteNumber * 8 + colorIndex * 2;
        colorNumber = uint16_t(gbcBackgroundPalettesData.read(gbcColorIndex + 1)) << 8 | uint16_t(gbcBackgroundPalettesData.read(gbcColorIndex));
    }
    else {
        switch (colorIndex) {
        case 0:
            colorNumber = registers.shadeForBackgroundColorNumber0;
            break;
        case 1:
            colorNumber = registers.shadeForBackgroundColorNumber1;
            break;
        case 2:
            colorNumber = registers.shadeForBackgroundColorNumber2;
            break;
        case 3:
            colorNumber = registers.shadeForBackgroundColorNumber3;
            break;
        default:
            break;
        }
    }
    return colorNumber;
}

cv::Vec3b GbGpu::getColor(uint16_t colorNumber) const
{
    if (gbCartridge.isGbcMode()) {
        GbcColor gbcColor = {.color = colorNumber};
        return cv::Vec3b((gbcColor.blue << 3) | (gbcColor.blue >> 2), (gbcColor.green << 3) | (gbcColor.green >> 2), (gbcColor.red << 3) | (gbcColor.red >> 2));
    }
    switch (colorNumber)
    {
    case 0:
        return cv::Vec3b(255, 255, 255);
    case 1:
        return cv::Vec3b(170, 170, 170);
    case 2:
        return cv::Vec3b(85, 85, 85);
    case 3:
        return cv::Vec3b(0, 0, 0);
    default:
        return cv::Vec3b(255, 255, 255);
    }
}

}  // namespace qgbemu
