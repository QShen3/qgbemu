/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <mutex>

#include <opencv2/core/mat.hpp>

namespace qgbemu
{

template<typename T = uint8_t>
class GpuInterface
{
    public:
        virtual ~GpuInterface() = default;

        virtual const cv::Mat& getOutput() const = 0;
        virtual std::unique_lock<std::mutex> outputSync() = 0;
        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void exec() = 0;
        virtual T readFromVideoRam(size_t address) const = 0;
        virtual void writeToVideoRam(size_t address, const T& value) = 0;
};

}  // namespace qgbemu
