/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>
#include <stdexcept>

#include "QGBEmu/Memory/MemoryInterface.h"

namespace qgbemu
{

template<typename T = uint8_t, size_t SIZE = 64>
class Rom : public MemoryInterface<T, SIZE>
{
    public:
        Rom();

        T read(size_t address) const override;
        void write(size_t address, const T& value) override;
        size_t size() const override;
        void load(const std::array<T, SIZE>& data) override;

    private:
        std::array<T, SIZE> data;
};

template<typename T, size_t SIZE>
Rom<T, SIZE>::Rom()
{
    data.fill(0);
}

template<typename T, size_t SIZE>
T Rom<T, SIZE>::read(size_t address) const
{
    return data.at(address);
}

template<typename T, size_t SIZE>
void Rom<T, SIZE>::write(size_t /* address */, const T& /* value */)
{
    throw std::runtime_error("This memory is readonly!");
}

template<typename T, size_t SIZE>
size_t Rom<T, SIZE>::size() const
{
    return data.size();
}

template<typename T, size_t SIZE>
void Rom<T, SIZE>::load(const std::array<T, SIZE>& newData)
{
    std::copy(newData.begin(), newData.end(), data.begin());
}

}  // namespace qgbemu
