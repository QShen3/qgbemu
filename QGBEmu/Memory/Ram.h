/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>

#include "QGBEmu/Memory/MemoryInterface.h"

namespace qgbemu
{

template<typename T = uint8_t, size_t SIZE = 64>
class Ram : public MemoryInterface<T, SIZE>
{
    public:
        Ram();

        T read(size_t address) const override;
        void write(size_t address, const T& value) override;
        size_t size() const override;
        void load(const std::array<T, SIZE>& data) override;

    private:
        std::array<T, SIZE> data;
};

template<typename T, size_t SIZE>
Ram<T, SIZE>::Ram()
{
    data.fill(0);
}

template<typename T, size_t SIZE>
T Ram<T, SIZE>::read(size_t address) const
{
    return data.at(address);
}

template<typename T, size_t SIZE>
void Ram<T, SIZE>::write(size_t address, const T& value)
{
    data.at(address) = value;
}

template<typename T, size_t SIZE>
size_t Ram<T, SIZE>::size() const
{
    return data.size();
}

template<typename T, size_t SIZE>
void Ram<T, SIZE>::load(const std::array<T, SIZE>& newData)
{
    std::copy(newData.begin(), newData.end(), data.begin());
}

}  // namespace qgbemu
