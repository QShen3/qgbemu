/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <utility>

#include <QtCore/QObject>

#include "QGBEmu/Cartridge/GbCartridgeInterface.h"
#include "QGBEmu/Gpu/GbGpu.h"
#include "QGBEmu/Memory/GbMemoryMapper.h"
#include "QGBEmu/Memory/Ram.h"

namespace qgbemu
{

GbMemoryMapper::GbMemoryMapper
(
    std::unique_ptr<GbCartridgeInterface> cartridge,
    std::unique_ptr<GbGpu> gpu
) :
    cartridge(std::move(cartridge)),
    gpu(std::move(gpu)),
    currentWorkRamBank(1)
{
    QObject::connect(this->gpu.get(), &GbGpu::vBlankEntered, [this](){
        registers.vBlankRequest = 1;
    });
    QObject::connect(this->gpu.get(), &GbGpu::statTriggered, [this](){
        registers.lcdRequest = 1;
    });
}

void GbMemoryMapper::reset()
{
    memset(&registers, 0, sizeof(registers));
    registers.interruptFlag = 0xE1;
}

uint8_t GbMemoryMapper::read(uint16_t address) const
{
    if (address <= 0x7FFF) {
        return cartridge->readFromRom(address);
    }
    if (address >= 0x8000 && address <= 0x9FFF) {
        return gpu->readFromVideoRam(address & 0x7FFF);
    }
    if (address >= 0xA000 && address <= 0xBFFF) {
        return cartridge->readFromRam(address & 0x5FFF);
    }
    if (address >= 0xC000 && address <= 0xCFFF) {
        return workRamBanks.at(0).read(address & 0x3FFF);
    }
    if (address >= 0xD000 && address <= 0xDFFF) {
        return workRamBanks.at(currentWorkRamBank).read(address & 0x2FFF);
    }
    if (address >= 0xE000 && address <= 0xFDFF) {
        return read(address & 0xDFFF);
    }
    if (address >= 0xFE00 && address <= 0xFE9F) {
        return gpu->readFromSpriteAttributeTable(address & 0x1FF);
    }
    if (address >= 0xFF00 && address <= 0xFF79) {
        try {
            return readRegister(address);
        }
        catch (const std::runtime_error&) {
            assert(false && "Undefined register");
            return 0xFF;
        }
    }
    if (address >= 0xFF80 && address <= 0xFFFE) {
        return internalHighRam.read(address & 0x7F);
    }
    if (address == 0xFFFF) {
            return registers.interruptEnabled;
    }
    return 0;
}

void GbMemoryMapper::write(uint16_t address, uint8_t value)
{
    if (address <= 0x7FFF) {
        return;
    }
    if (address >= 0x8000 && address <= 0x9FFF) {
        gpu->writeToVideoRam(address & 0x7FFF, value);
    }
    else if (address >= 0xA000 && address <= 0xBFFF) {
        cartridge->writeToRam(address & 0x5FFF, value);
    }
    else if (address >= 0xC000 && address <= 0xCFFF) {
        workRamBanks.at(0).write(address & 0x3FFF, value);
    }
    else if (address >= 0xD000 && address <= 0xDFFF) {
        workRamBanks.at(currentWorkRamBank).write(address & 0x2FFF, value);
    }
    else if (address >= 0xE000 && address <= 0xFDFF) {
        write(address & 0xDFFF, value);
    }
    else if (address >= 0xFE00 && address <= 0xFE9F) {
        gpu->writeToSpriteAttributeTable(address & 0x1FF, value);
    }
    else if (address >= 0xFF00 && address <= 0xFF79) {
        try {
            writeRegister(address, value);
        }
        catch (const std::runtime_error&) {
            assert(false && "Undefined register");
        }
    }
    else if (address >= 0xFF80 && address <= 0xFFFE) {
        internalHighRam.write(address & 0x7F, value);
    }
    else if (address == 0xFFFF) {
        registers.interruptEnabled = value;
    }
}

void GbMemoryMapper::loadCartridge(const char* filePath)
{
    cartridge->load(filePath);
}

void GbMemoryMapper::writeRegister(size_t address, uint8_t value)
{
    switch (address)
    {
    case 0xFF0F:
        registers.interruptFlag = value;
        break;
    case 0xFF40:
        gpu->registers.lcdControl = value;
        break;
    case 0xFF41:
        value &= 0b11111000;
        value |= (gpu->registers.lcdStatus &= 0b00000111);
        gpu->registers.lcdStatus = value;
        break;
    case 0xFF42:
        gpu->registers.scrollY = value;
        break;
    case 0xFF43:
        gpu->registers.scrollX = value;
        break;
    case 0xFF44:
        gpu->registers.lcdYCoordinate = gpu->registers.lcdYCoordinate;
        break;
    case 0xFF45:
        gpu->registers.lcdYCoordinateCompare = value;
        break;
    case 0xFF46:
        gpu->registers.dMATransferAndStartAddress = value;
        break;
    case 0xFF47:
        gpu->registers.backgroundPaletteData = value;
        break;
    case 0xFF48:
        gpu->registers.spritePalette0Data = value;
        break;
    case 0xFF49:
        gpu->registers.spritePalette1Data = value;
        break;
    case 0xFF4A:
        gpu->registers.windowYPosition = value;
        break;
    case 0xFF4B:
        gpu->registers.windowXPosition = value;
        break;
    case 0xFF4F:
        gpu->registers.videoRamBank = value;
        break;
    case 0xFF51:
        gpu->registers.newDMASourceHigh = value;
        break;
    case 0xFF52:
        gpu->registers.newDMASourceLow = value;
        break;
    case 0xFF53:
        gpu->registers.newDMADestinationHigh = value;
        break;
    case 0xFF54:
        gpu->registers.newDMADestinationLow = value;
        break;
    case 0xFF55:
        gpu->registers.newDMALength = value;
        break;
    case 0xFF68:
        gpu->registers.backgroundColorPaletteSpecification = value;
        break;
    case 0xFF69:
        if (gpu->registers.modeFlag != 3 || gpu->registers.lcdEnable == 0) {
            gpu->writeBackgroundColorPaletteData(value);
        }
        break;
    case 0xFF6A:
        gpu->registers.spriteColorPaletteSpecification = value;
        break;
    case 0xFF6B:
        if (gpu->registers.modeFlag != 3 || gpu->registers.lcdEnable == 0) {
            gpu->writeSpriteColorPaletteData(value);
        }
        break;
    default:
        return;
    }
}

uint8_t GbMemoryMapper::readRegister(size_t address) const
{
    switch (address)
    {
    case 0xFF0F:
        return registers.interruptFlag;
    case 0xFF40:
        return gpu->registers.lcdControl;
    case 0xFF41:
        return gpu->registers.lcdStatus;
    case 0xFF42:
        return gpu->registers.scrollY;
    case 0xFF43:
        return gpu->registers.scrollX;
    case 0xFF44:
        return gpu->registers.lcdYCoordinate;
    case 0xFF45:
        return gpu->registers.lcdYCoordinateCompare;
    case 0xFF46:
        return gpu->registers.dMATransferAndStartAddress;
    case 0xFF47:
        return gpu->registers.backgroundPaletteData;
    case 0xFF48:
        return gpu->registers.spritePalette0Data;
    case 0xFF49:
        return gpu->registers.spritePalette1Data;
    case 0xFF4A:
        return gpu->registers.windowYPosition;
    case 0xFF4B:
        return gpu->registers.windowXPosition;
    case 0xFF4F:
        return gpu->registers.videoRamBank;
    case 0xFF51:
        return gpu->registers.newDMASourceHigh;
    case 0xFF52:
        return gpu->registers.newDMASourceLow;
    case 0xFF53:
        return gpu->registers.newDMADestinationHigh;
    case 0xFF54:
        return gpu->registers.newDMADestinationLow;
    case 0xFF55:
        return gpu->registers.newDMALength;
    case 0xFF68:
        return gpu->registers.backgroundColorPaletteSpecification;
    case 0xFF69:
        if (gpu->registers.modeFlag != 3 || gpu->registers.lcdEnable == 0) {
            return gpu->registers.backgroundColorPaletteData;
        }
        else {
            return 0xFF;
        }
    case 0xFF6A:
        return gpu->registers.spriteColorPaletteSpecification;
    case 0xFF6B:
        if (gpu->registers.modeFlag != 3 || gpu->registers.lcdEnable == 0) {
            return gpu->registers.spriteColorPaletteData;
        }
        else {
            return 0xFF;
        }
    default:
        return 0xFF;
    }
}

}  // namespace qgbemu
