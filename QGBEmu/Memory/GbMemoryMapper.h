/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <memory>

#include "QGBEmu/Cartridge/GbCartridgeInterface.h"
#include "QGBEmu/Gpu/GbGpu.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

class GbCpu;

class GbMemoryMapper
{
    public:
        GbMemoryMapper() = delete;
        explicit GbMemoryMapper
        (
            std::unique_ptr<GbCartridgeInterface> cartridge,
            std::unique_ptr<GbGpu> gpu
        );
        ~GbMemoryMapper() = default;

        void reset();

        uint8_t read(uint16_t address) const;
        void write(uint16_t address, uint8_t value);

        void loadCartridge(const char* filePath);

        struct {
            union {
                struct {
                    uint8_t vBlankRequest : 1;
                    uint8_t lcdRequest : 1;
                    uint8_t timerRequest : 1;
                    uint8_t serialRequest : 1;
                    uint8_t joyPadRequest : 1;
                    uint8_t interruptFlagReserved : 3;
                };
                uint8_t interruptFlag;
            };
            union {
                struct {
                    uint8_t vBlankEnabled : 1;
                    uint8_t lcdEnabled : 1;
                    uint8_t timerEnabled : 1;
                    uint8_t serialEnabled : 1;
                    uint8_t joyPadEnabled : 1;
                    uint8_t interruptEnabledReserved : 3;
                };
                uint8_t interruptEnabled;
            };
        } registers;

    private:
        friend GbCpu;
        uint8_t readRegister(size_t address) const;
        void writeRegister(size_t address, uint8_t value);

        std::unique_ptr<GbCartridgeInterface> cartridge;
        std::unique_ptr<GbGpu> gpu;
        std::array<Ram<uint8_t, 0x1000>, 8> workRamBanks;
        Ram<uint8_t, 0x7F> internalHighRam;               // 0xFF80 ~ 0xFFFE

        size_t currentWorkRamBank;
};

}  // namespace qgbemu
