/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>

namespace qgbemu
{

template<typename T = uint8_t, size_t SIZE = 64>
class MemoryInterface
{
    public:
        virtual ~MemoryInterface() = default;

        virtual T read(size_t address) const = 0;
        virtual void write(size_t address, const T& value) = 0;
        virtual size_t size() const = 0;
        virtual void load(const std::array<T, SIZE>& data) = 0;
};

}  // namespace qgbemu
