/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <QtCore/QtGlobal>
#include <QtGui/QImage>
#include <QtGui/QPainter>
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "QGBEmu/Screen/Screen.h"

namespace qgbemu
{

Screen::Screen(QQuickPaintedItem *parent) : QQuickPaintedItem(parent)
{
}

std::function<const cv::Mat&()> Screen::getOutputFunction() const
{
    return getOutput;
}

void Screen::setGetOutputFunction(std::function<const cv::Mat&()> function)
{
    getOutput = function;
    emit outputFunctionChanged(getOutput);
}

std::function<std::unique_lock<std::mutex>()> Screen::outputSyncFunction() const
{
    return outputSync;
}

void Screen::setOutputSyncFunction(std::function<std::unique_lock<std::mutex>()> function)
{
    outputSync = function;
    emit outputSyncFunctionChanged(outputSync);
}

void Screen::paint(QPainter *painter)
{
    if (outputSync && getOutput) {
        auto sourceLock = outputSync();
        auto frameMat = getOutput();
        QImage frame(frameMat.data, frameMat.cols, frameMat.rows, static_cast<int>(frameMat.step), QImage::Format_BGR888);
        painter->drawImage(boundingRect(), frame);
    }
}

}  // namespace qgbemu
