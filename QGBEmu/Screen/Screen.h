/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstdint>
#include <functional>
#include <mutex>

#include <opencv2/core/mat.hpp>
#include <QtCore/QObject>
#include <QtGui/QPainter>
#include <QtQuick/QQuickPaintedItem>

#include "QGBEmu/Gpu/GpuInterface.h"

namespace qgbemu
{

class Screen : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(std::function<const cv::Mat&()> getOutputFunction READ getOutputFunction WRITE setGetOutputFunction NOTIFY outputFunctionChanged)
    Q_PROPERTY(std::function<std::unique_lock<std::mutex>()> outputSyncFunction READ outputSyncFunction WRITE setOutputSyncFunction NOTIFY outputSyncFunctionChanged)

    public:
        explicit Screen(QQuickPaintedItem *parent = nullptr);
        virtual ~Screen() = default;

        std::function<const cv::Mat&()> getOutputFunction() const;
        void setGetOutputFunction(std::function<const cv::Mat&()> function);

        std::function<std::unique_lock<std::mutex>()> outputSyncFunction() const;
        void setOutputSyncFunction(std::function<std::unique_lock<std::mutex>()> function);

        void paint(QPainter *painter) override;

    signals:
        void outputFunctionChanged(std::function<const cv::Mat&()>);
        void outputSyncFunctionChanged(std::function<std::unique_lock<std::mutex>()>);

    private:
        std::function<const cv::Mat&()> getOutput;
        std::function<std::unique_lock<std::mutex>()> outputSync;
};

}  // namespace qgbemu
