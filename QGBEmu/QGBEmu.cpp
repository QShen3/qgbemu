/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <utility>

#include <QtCore/QDir>
#include <QtCore/QUrl>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>

#include "QGBEmu/Cpu/GbCpu.h"
#include "QGBEmu/Gpu/GbGpu.h"
#include "QGBEmu/QGBEmu.h"
#include "QGBEmu/Screen/Screen.h"

namespace qgbemu
{

QGBEmu::QGBEmu(const QQmlApplicationEngine& engine)
{
    auto cartridge = std::make_unique<GbCartridge>(gbMbcFactory);
    auto gpu = std::make_unique<GbGpu>(*cartridge);
    engine.rootContext()->setContextProperty("gpu", gpu.get());
    engine.rootContext()->setContextProperty("qgbemu", this);

    auto memoryMapper = std::make_unique<GbMemoryMapper>(std::move(cartridge), std::move(gpu));
    cpu = std::make_unique<GbCpu>(std::move(memoryMapper), static_cast<uint32_t>(std::pow(10, 9) / std::pow(2, 23)));
    qmlRegisterType<Screen>("QGBEmu", 1, 0, "Screen");
}

void QGBEmu::loadCartridge(const char *filePath)
{
    (void)filePath;
    cpu->stop();
    cpu->loadCartridge(filePath);
    cpu->start();
}

void QGBEmu::loadCartridge(const QUrl& filePath)
{
    auto path = QDir::toNativeSeparators(filePath.toLocalFile());
    loadCartridge(path.toLocal8Bit().constData());
}

}  // namespace qgbemu
