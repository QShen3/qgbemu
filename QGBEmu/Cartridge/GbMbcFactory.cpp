/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cstdint>
#include <memory>
#include <utility>
#include <vector>

#include "QGBEmu/Cartridge/GbMbcFactory.h"
#include "QGBEmu/Cartridge/Mbc/GbMbc1.h"
#include "QGBEmu/Cartridge/Mbc/GbMbc2.h"
#include "QGBEmu/Cartridge/Mbc/GbMbc3.h"
#include "QGBEmu/Cartridge/Mbc/GbMbc5.h"
#include "QGBEmu/Cartridge/Mbc/GbMbc6.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcCircuit.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

std::unique_ptr<GbMbcInterface> GbMbcFactory::create(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) const
{
    std::unique_ptr<GbMbcInterface> mbc;

    if (type == 0x0 || type == 0x8 || type == 0x9) {
        mbc = std::make_unique<GbMbcCircuit>(std::move(romBanks), std::move(ramBanks), type);
    }
    else if (type == 0x1 || type == 0x2 || type == 0x3) {
        mbc = std::make_unique<GbMbc1>(std::move(romBanks), std::move(ramBanks), type);
    }
    else if (type == 0x5 || type == 0x6) {
        mbc = std::make_unique<GbMbc2>(std::move(romBanks), std::move(ramBanks), type);
    }
    else if (type == 0xF || type == 0x10 || type == 0x11 || type == 0x12 || type == 0x13) {
        mbc = std::make_unique<GbMbc3>(std::move(romBanks), std::move(ramBanks), type);
    }
    else if (type == 0x19 || type == 0x1A || type == 0x1B || type == 0x1C || type == 0x1D || type == 0x1E) {
        mbc = std::make_unique<GbMbc5>(std::move(romBanks), std::move(ramBanks), type);
    }
    else if (type == 0x20) {
        mbc = std::make_unique<GbMbc6>(std::move(romBanks), std::move(ramBanks), type);
        // MBC6 is not complete
        throw std::runtime_error("This game is not supported!");
    }
    else {
        throw std::runtime_error("This game is not supported!");
    }

    return mbc;
}

}  // namespace qgbemu
