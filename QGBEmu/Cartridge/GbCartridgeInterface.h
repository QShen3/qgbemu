/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include "QGBEmu/Cartridge/CartridgeInterface.h"

namespace qgbemu
{

    using GbCartridgeInterface = CartridgeInterface<uint8_t>;

}  // namespace qgbemu
