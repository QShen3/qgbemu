/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>

namespace qgbemu
{

template<typename T = uint8_t>
class CartridgeInterface
{
    public:
        virtual ~CartridgeInterface() = default;

        virtual void load(const char* filePath) = 0;

        virtual T readFromRom(size_t address) = 0;

        virtual T readFromRam(size_t address) = 0;
        virtual void writeToRam(size_t address, T value) = 0;
};

}  // namespace qgbemu
