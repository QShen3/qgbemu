/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbc5.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

GbMbc5::GbMbc5(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type)
{
}

void GbMbc5::setControlRegister(size_t address, uint8_t value)
{
    (void)type;
    if (address <= 0x1FFF) {
        registers.ramEnable = value;
    }
    else if (address >= 0x2000 && address <= 0x2FFF) {
        registers.romBankNumberLow = value;
    }
    else if (address >= 0x3000 && address <= 0x3FFF) {
        registers.romBankNumberHigh = (value & 0b1);
    }
    else if (address >= 0x4000 && address <= 0x5FFF) {
        registers.ramBankNumber = (value & 0b1111);
    }
}

uint8_t GbMbc5::readFromRom(size_t address) const
{
    uint16_t bankNumber = 0;
    if (address < 0x4000) {
        return romBanks.at(bankNumber).read(address);
    }
    bankNumber = registers.romBankNumberHigh;
    bankNumber = (bankNumber << 8) | registers.romBankNumberLow;
    bankNumber &= (0xFFFF >> uint16_t(16 - log2(romBanks.size() / 0x4000)));
    return romBanks.at(bankNumber).read(address & 0xBFFF);
}

uint8_t GbMbc5::readFromRam(size_t address) const
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return 0xFF;
    }
    if (ramBanks.size() == 0) {
        assert(false && "No Ram available");
        return 0xFF;
    }

    uint8_t bankNumber = 0;
    if (ramBanks.size() == 0x800) {
        bankNumber = 0;
        address &= 0x7FF;
    }
    else {
        bankNumber = registers.ramBankNumber & (0xFF >> uint8_t(8 - log2(ramBanks.size() / 0x2000)));
    }
    return ramBanks.at(bankNumber).read(address);
}

void GbMbc5::writeToRam(size_t address, uint8_t value)
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return;
    }
    if (ramBanks.empty()) {
        assert(false && "No Ram available");
        return;
    }

    uint8_t bankNumber = 0;
    if (ramBanks.size() == 0x800) {
        bankNumber = 0;
        address &= 0x7FF;
    }
    else {
        bankNumber = registers.ramBankNumber & (0xFF >> uint8_t(8 - log2(ramBanks.size() / 0x2000)));
    }
    ramBanks.at(bankNumber).write(address, value);
}

}  // namespace qgbemu
