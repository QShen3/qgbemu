/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>

namespace qgbemu
{

template<typename T = uint8_t>
class MbcInterface
{
    public:
        virtual ~MbcInterface() = default;

        virtual void setControlRegister(size_t address, T value) = 0;

        virtual T readFromRom(size_t address) const = 0;

        virtual T readFromRam(size_t address) const = 0;
        virtual void writeToRam(size_t address, T value) = 0;
};

}  // namespace qgbemu
