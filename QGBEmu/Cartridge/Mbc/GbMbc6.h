/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <array>
#include <stddef.h>
#include <stdint.h>
#include <vector>

#include "QGBEmu/Cartridge/GbCartridge.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

class GbMbc6 : public GbMbcInterface
{
    public:
        struct Registers
        {
            uint8_t ramEnable;
            uint8_t ramBankANumber;
            uint8_t ramBankBNumber;
            uint8_t flashEnable;
            uint8_t flashWriteEnable;
            uint8_t romOrFlashBankANumber;
            uint8_t romOrFlashBankASelect;
            uint8_t romOrFlashBankBNumber;
            uint8_t romOrFlashBankBSelect;
        };

        GbMbc6() = delete;
        explicit GbMbc6(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type);
        ~GbMbc6() = default;

        void setControlRegister(size_t address, uint8_t value) override;

        uint8_t readFromRom(size_t address) const override;

        uint8_t readFromRam(size_t address) const override;
        void writeToRam(size_t address, uint8_t value) override;

        Registers registers;

    private:
        std::vector<Rom<uint8_t, romBankSize>> romBanks;
        std::vector<Ram<uint8_t, ramBankSize>> ramBanks;
        uint8_t type;
        std::array<Ram<uint8_t, 0x2000>, 128> flash;
};

}  // namespace qgbemu
