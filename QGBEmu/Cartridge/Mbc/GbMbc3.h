/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>
#include <vector>

#include "QGBEmu/Cartridge/GbCartridge.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"
#include "QGBEmu/Utility/RealTimeClockInterface.h"

namespace qgbemu
{

class GbMbc3 : public GbMbcInterface
{
    public:
        struct Registers
        {
            uint8_t ramEnable;
            uint8_t romBankNumber;
            uint8_t ramBankNumber;

            uint8_t latchClockData;
            uint8_t realTimeClockSeconds;
            uint8_t realTimeClockMinutes;
            uint8_t realTimeClockHours;
            uint8_t realTimeClockDayLow;
            uint8_t realTimeClockDayHigh;
        };

        GbMbc3() = delete;
        explicit GbMbc3(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type);
        ~GbMbc3() = default;

        void setControlRegister(size_t address, uint8_t value) override;
        void updateRealTimeClockRegisters();

        uint8_t readFromRom(size_t address) const override;

        uint8_t readFromRam(size_t address) const override;
        void writeToRam(size_t address, uint8_t value) override;

        Registers registers;

    private:
        std::vector<Rom<uint8_t, romBankSize>> romBanks;
        std::vector<Ram<uint8_t, ramBankSize>> ramBanks;
        uint8_t type;
        std::unique_ptr<RealTimeClockInterface<>> realTimeClock;

        uint8_t previousLatchClockData;
        bool realTimeClocklatched;
};

}  // namespace qgbemu
