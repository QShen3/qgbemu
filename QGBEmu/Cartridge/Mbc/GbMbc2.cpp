/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbc2.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

GbMbc2::GbMbc2(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type)
{
    registers.ramEnable = 0;
    registers.romBankNumber = 1;
}

void GbMbc2::setControlRegister(size_t address, uint8_t value)
{
    (void)type;
    if (address <= 0x3FFF) {
        if (((address >> 8) & 1) == 1) {
            registers.romBankNumber = value;
        }
        else {
            registers.ramEnable = value;
        }
    }
}

uint8_t GbMbc2::readFromRom(size_t address) const
{
    uint8_t bankNumber = 0;
    if (address < 0x4000) {
        bankNumber = 0;
        return romBanks.at(bankNumber).read(address);
    }
    switch (romBanks.size()) {
        case 0x8000:
            bankNumber = 1;
            break;
        case 0x1'0000:
            bankNumber = (registers.romBankNumber & 0b11);
            bankNumber = bankNumber == 0 ? 1 : bankNumber;
            break;
        case 0x2'0000:
            bankNumber = (registers.romBankNumber & 0b111);
            bankNumber = bankNumber == 0 ? 1 : bankNumber;
            break;
        case 0x4'0000:
            bankNumber = (registers.romBankNumber & 0b1111);
            bankNumber = bankNumber == 0 ? 1 : bankNumber;
            break;
    }
    return romBanks.at(bankNumber).read(address & 0xBFFF);
}

uint8_t GbMbc2::readFromRam(size_t address) const
{
    if (registers.ramEnable != 0x0A) {
        assert(false && "Ram is disabled");
        return 0xFF;
    }
    return ramBanks.at(0).read(address) & 0xF;
}

void GbMbc2::writeToRam(size_t address, uint8_t value)
{
    if (registers.ramEnable != 0x0A) {
        assert(false && "Ram is disabled");
        return;
    }
    ramBanks.at(0).write(address, value & 0xF);
}

}  // namespace qgbemu
