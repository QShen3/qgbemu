/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include <QtCore/QTimer>

#include "QGBEmu/Cartridge/Mbc/GbMbc3.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"
#include "QGBEmu/Utility/GbRealTimeClock.h"

namespace qgbemu
{

GbMbc3::GbMbc3(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type),
    realTimeClocklatched(false)
{
    // Todo: Init the value of clock registers.
    auto timer = std::make_unique<QTimer>();
    realTimeClock = std::make_unique<GbRealTimeClock<>>(std::move(timer));
    realTimeClock->setFunction(std::bind(&GbMbc3::updateRealTimeClockRegisters, this));
    if ((registers.realTimeClockDayHigh & 0b1000000) > 0) {
        realTimeClock->stop();
    }
    else {
        realTimeClock->start();
    }
}

void GbMbc3::setControlRegister(size_t address, uint8_t value)
{
    (void)type;
    if (address <= 0x1FFF) {
        registers.ramEnable = value;
    }
    else if (address >= 0x2000 && address <= 0x3FFF) {
        registers.romBankNumber = (value & 0b1111111);
    }
    else if (address >= 0x4000 && address <= 0x5FFF) {
        registers.ramBankNumber = (value & 0b1111);
    }
    else if (address >= 0x6000 && address <= 0x7FFF) {
        registers.latchClockData = value;
        if (value == 1 && previousLatchClockData == 0) {
            if (realTimeClocklatched) {
                realTimeClock->setFunction(std::bind(&GbMbc3::updateRealTimeClockRegisters, this));
                realTimeClocklatched = false;
            }
            else {
                realTimeClock->clearFunction();
                realTimeClocklatched = true;
            }
        }
    }
}

void GbMbc3::updateRealTimeClockRegisters()
{
    registers.realTimeClockSeconds = realTimeClock->getSeconds();
    registers.realTimeClockMinutes = realTimeClock->getMinutes();
    registers.realTimeClockHours = realTimeClock->getHours();
    uint16_t days = realTimeClock->getDays();
    registers.realTimeClockDayLow = uint8_t(days);
    registers.realTimeClockDayHigh |= ((days >> 8) & 1);
    registers.realTimeClockDayHigh |= (realTimeClock->getCarry() ? 0b10000000 : 0);
}

uint8_t GbMbc3::readFromRom(size_t address) const
{
    uint8_t bankNumber = 0;
    if (address < 0x4000) {
        return romBanks.at(bankNumber).read(address);
    }
    bankNumber = registers.romBankNumber & (0xFF >> uint8_t(8 - log2(romBanks.size() / 0x4000)));
    bankNumber = bankNumber == 0 ? 1 : bankNumber;
    return romBanks.at(bankNumber).read(address & 0xBFFF);
}

uint8_t GbMbc3::readFromRam(size_t address) const
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return 0xFF;
    }

    uint8_t bankNumber = 0;
    if (registers.ramBankNumber <= 0x3) {
        switch (ramBanks.size()) {
            case 0:
                assert(false && "No Ram available");
                return 0xFF;
            case 0x800:
                bankNumber = 0;
                address &= 0x7FF;
                break;
            case 0x2000:
                bankNumber = 0;
                break;
            case 0x8000:
                bankNumber = registers.ramBankNumber;
                break;
        }
        return ramBanks.at(bankNumber).read(address);
    }
    if (registers.ramBankNumber >= 0x8 && registers.ramBankNumber <= 0xC) {
        switch (registers.ramBankNumber)
        {
            case 0x8:
                return registers.realTimeClockSeconds;
            case 0x9:
                return registers.realTimeClockMinutes;
            case 0xA:
                return registers.realTimeClockHours;
            case 0xB:
                return registers.realTimeClockDayLow;
            case 0xC:
                return registers.realTimeClockDayHigh;
            default:
                return 0xFF;
        }
    }
    assert(false && "Unknown Ram bank");
    return 0xFF;
}

void GbMbc3::writeToRam(size_t address, uint8_t value)
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return;
    }

    uint8_t bankNumber = 0;
    if (registers.ramBankNumber <= 0x3) {
        switch (ramBanks.size()) {
            case 0:
                assert(false && "No Ram available");
                return;
            case 0x800:
                bankNumber = 0;
                address &= 0x7FF;
                break;
            case 0x2000:
                bankNumber = 0;
                break;
            case 0x8000:
                bankNumber = registers.ramBankNumber;
                break;
        }
        ramBanks.at(bankNumber).write(address, value);
    }
    else if (registers.ramBankNumber >= 0x8 && registers.ramBankNumber <= 0xC) {
        switch (registers.ramBankNumber)
        {
            case 0x8:
                registers.realTimeClockSeconds = value;
                realTimeClock->setSeconds(value);
                break;
            case 0x9:
                registers.realTimeClockMinutes = value;
                realTimeClock->setMinutes(value);
                break;
            case 0xA:
                registers.realTimeClockHours = value;
                realTimeClock->setHours(value);
                break;
            case 0xB:
                registers.realTimeClockDayLow = value;
                realTimeClock->setDays((realTimeClock->getDays() & 0xFF00) | value);
                break;
            case 0xC:
                registers.realTimeClockDayHigh = value;
                uint16_t dayHigh = uint16_t(value & 0x01) << 8;
                realTimeClock->setDays((realTimeClock->getDays() & 0b1111111011111111) | dayHigh);
                bool carry = (value & 0b10000000) > 0 ? true : false;
                realTimeClock->setCarry(carry);
                if ((registers.realTimeClockDayHigh & 0b1000000) > 0) {
                    realTimeClock->stop();
                }
                else {
                    realTimeClock->start();
                }
                break;
        }
    }
}

}  // namespace qgbemu
