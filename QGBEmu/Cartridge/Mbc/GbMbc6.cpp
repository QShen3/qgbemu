/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cmath>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbc6.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

GbMbc6::GbMbc6(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type)
{
}

void GbMbc6::setControlRegister(size_t address, uint8_t value)
{
    (void)type;
    if (address <= 0x3FF) {
        registers.ramEnable = value;
    }
    else if (address >= 0x400 && address <= 0x7FF) {
        registers.ramBankANumber = value;
    }
    else if (address >= 0x800 && address <= 0xBFF) {
        registers.ramBankBNumber = value;
    }
    else if (address >= 0xC00 && address <= 0xFFF) {
        if (registers.flashWriteEnable == 1) {
            registers.flashEnable = (value & 0b1);
        }
    }
    else if (address == 0x1000) {
        registers.flashWriteEnable = (value & 0b1);
    }
    else if (address >= 0x2000 && address <= 0x27FF) {
        registers.romOrFlashBankANumber = value;
    }
    else if (address >= 0x2800 && address <= 0x2FFF) {
        registers.romOrFlashBankASelect = value;
    }
    else if (address >= 0x3000 && address <= 0x37FF) {
        registers.romOrFlashBankBNumber = value;
    }
    else if (address >= 0x3800 && address <= 0x3FFF) {
        registers.romOrFlashBankBSelect = value;
    }
}

uint8_t GbMbc6::readFromRom(size_t address) const
{
    uint8_t bankNumber = 0;
    if (address < 0x4000) {
        return romBanks.at(bankNumber).read(address);
    }
    if (address >= 0x4000 && address <= 0x5FFF) {
        if (registers.romOrFlashBankBSelect == 0) {
            bankNumber = bankNumber & (0xFF >> uint8_t(8 - log2(romBanks.size() / 0x4000)));
            return romBanks.at(bankNumber).read((address & 0xBFFF) + 0x2000);
        }
        if (registers.romOrFlashBankBSelect == 8) {
            if (registers.flashEnable) {
                bankNumber = bankNumber & 0b01111111;
                return flash.at(bankNumber).read((address & 0xBFFF) + 0x2000);
            }
        }
    }
    else if (address >= 0x6000) {
        if (registers.romOrFlashBankBSelect == 0) {
            bankNumber = bankNumber & (0xFF >> uint8_t(8 - log2(romBanks.size() / 0x4000)));
            return romBanks.at(bankNumber).read(address & 0x9FFF);
        }
        if (registers.romOrFlashBankBSelect == 8) {
            if (registers.flashEnable) {
                bankNumber = bankNumber & 0b1111111;
                return flash.at(bankNumber).read(address & 0xB9FFF);
            }
        }
    }
    return 0xFF;
}

uint8_t GbMbc6::readFromRam(size_t /* address */) const
{
    return 0;
}

void GbMbc6::writeToRam(size_t /* address */, uint8_t /* value */)
{
}

}  // namespace qgbemu
