/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbcCircuit.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

GbMbcCircuit::GbMbcCircuit(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type)
{
}

void GbMbcCircuit::setControlRegister(size_t address, uint8_t value)
{
    (void)address;
    (void)value;
}

uint8_t GbMbcCircuit::readFromRom(size_t address) const
{
    if (address < 0x4000) {
        return romBanks.at(0).read(address);
    }
    return romBanks.at(1).read(address & 0xBFFF);
}

uint8_t GbMbcCircuit::readFromRam(size_t address) const
{
    if (type != 0x0) {
        return ramBanks.at(0).read(address);
    }
    assert(false && "No Ram available");
    return 0xFF;
}

void GbMbcCircuit::writeToRam(size_t address, uint8_t value)
{
    if (type != 0x0) {
        ramBanks.at(0).write(address, value);
    }
    else {
        assert(false && "No Ram available");
        return;
    }
}

}  // namespace qgbemu
