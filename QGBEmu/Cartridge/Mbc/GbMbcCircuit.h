/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/GbCartridge.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

class GbMbcCircuit : public GbMbcInterface
{
    public:
        GbMbcCircuit() = delete;
        explicit GbMbcCircuit(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type);
        ~GbMbcCircuit() = default;

        void setControlRegister(size_t address, uint8_t value) override;

        uint8_t readFromRom(size_t address) const override;

        uint8_t readFromRam(size_t address) const override;
        void writeToRam(size_t address, uint8_t value) override;

    private:
        std::vector<Rom<uint8_t, romBankSize>> romBanks;
        std::vector<Ram<uint8_t, ramBankSize>> ramBanks;
        uint8_t type;
};

}  // namespace qgbemu
