/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include "QGBEmu/Cartridge/Mbc/MbcInterface.h"

namespace qgbemu
{

constexpr size_t romBankSize = 0x4000;
constexpr size_t ramBankSize = 0x2000;

using GbMbcInterface = MbcInterface<uint8_t>;

}  // namespace qgbemu
