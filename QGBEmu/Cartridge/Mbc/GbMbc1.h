/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

class GbMbc1 : public GbMbcInterface
{
    public:
        struct Registers
        {
            uint8_t ramEnable;
            uint8_t romBankNumber;
            uint8_t ramBankNumber;
            uint8_t romRamModeSelect;
        };

        GbMbc1() = delete;
        explicit GbMbc1(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type);
        ~GbMbc1() = default;

        void setControlRegister(size_t address, uint8_t value) override;

        uint8_t readFromRom(size_t address) const override;

        uint8_t readFromRam(size_t address) const override;
        void writeToRam(size_t address, uint8_t value) override;

        Registers registers;

    private:
        uint8_t computeRomBankNumber(size_t address) const;
        uint8_t computeRamBankNumber() const;

        std::vector<Rom<uint8_t, romBankSize>> romBanks;
        std::vector<Ram<uint8_t, ramBankSize>> ramBanks;
        uint8_t type;
};

}  // namespace qgbemu
