/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbc1.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

GbMbc1::GbMbc1(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) :
    romBanks(romBanks), ramBanks(ramBanks), type(type)
{
    registers.ramEnable = 0;
    registers.romBankNumber = 0;
    registers.ramBankNumber = 0;
    registers.romRamModeSelect = 0;
}

void GbMbc1::setControlRegister(size_t address, uint8_t value)
{
    (void)type;
    if (address <= 0x1FFF) {
        registers.ramEnable = value;
    }
    else if (address >= 0x2000 && address <= 0x3FFF) {
        registers.romBankNumber = value;
    }
    else if (address >= 0x4000 && address <= 0x5FFF) {
        registers.ramBankNumber = (value & 0b11);
    }
    else if (address >= 0x6000 && address <= 0x7FFF) {
        registers.romRamModeSelect = (value & 0b1);
    }
}

uint8_t GbMbc1::readFromRom(size_t address) const
{
    uint8_t bankNumber = computeRomBankNumber(address);
    if (address < 0x4000) {
        return romBanks.at(bankNumber).read(address);
    }
    return romBanks.at(bankNumber).read(address & 0xBFFF);
}

uint8_t GbMbc1::readFromRam(size_t address) const
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return 0xFF;
    }
    uint8_t bankNumber = computeRamBankNumber();
    if (registers.romRamModeSelect != 0) {
        if (ramBanks.size() == 0x800) {
            address &= 0x7FF;
        }
    }
    return ramBanks.at(bankNumber).read(address);
}

void GbMbc1::writeToRam(size_t address, uint8_t value)
{
    if ((registers.ramEnable & 0b1111)!= 0x0A) {
        assert(false && "Ram is disabled");
        return;
    }
    uint8_t bankNumber = computeRamBankNumber();
    if (registers.romRamModeSelect != 0) {
        if (ramBanks.size() == 0x800) {
            address &= 0x7FF;
        }
    }
    ramBanks.at(bankNumber).write(address, value);
}

uint8_t GbMbc1::computeRomBankNumber(size_t address) const
{
    uint8_t bankNumber = 0;

    if (address < 0x4000) {
        if (registers.romRamModeSelect == 0) {
            bankNumber = 0;
        }
        else {
            bankNumber = 0 | ((registers.ramBankNumber & 0b11) << 5);
        }
    }
    else {
        switch (romBanks.size())
        {
            case 2:
                bankNumber = 1;
                break;
            case 4:
                bankNumber = (registers.romBankNumber & 0b11);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                break;
            case 8:
                bankNumber = (registers.romBankNumber & 0b111);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                break;
            case 16:
                bankNumber = (registers.romBankNumber & 0b1111);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                break;
            case 32:
                bankNumber = (registers.romBankNumber & 0b11111);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                break;
            case 64:
                bankNumber = (registers.romBankNumber & 0b11111);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                bankNumber |= (registers.ramBankNumber & 0b1) << 5;
                break;
            case 128:
                bankNumber = (registers.romBankNumber & 0b11111);
                bankNumber = bankNumber == 0 ? 1 : bankNumber;
                bankNumber |= (registers.ramBankNumber & 0b11) << 5;
                break;
            default:
                throw std::runtime_error("Invalid address!");
                break;
        }
    }

    return bankNumber;
}

uint8_t GbMbc1::computeRamBankNumber() const
{
    uint8_t bankNumber = 0;
    if (registers.romRamModeSelect == 0) {
        bankNumber = 0;
    }
    else {
        switch (ramBanks.size()) {
            case 0:
                assert(false && "No Ram available");
                break;
            case 1:
                bankNumber = 0;
                break;
            case 4:
                bankNumber = (registers.ramBankNumber & 0b11);
                break;
            default:
                throw std::runtime_error("Invalid address!");
                break;
        }
    }

    return bankNumber;
}

}  // namespace qgbemu
