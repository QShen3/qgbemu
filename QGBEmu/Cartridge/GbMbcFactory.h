/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include "QGBEmu/Cartridge/GbMbcFactoryInterface.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Rom.h"
#include "QGBEmu/Memory/Ram.h"

namespace qgbemu
{

class GbCartridge;

class GbMbcFactory : public GbMbcFactoryInterface
{
    public:
        GbMbcFactory() = default;
        ~GbMbcFactory() = default;

        std::unique_ptr<GbMbcInterface> create(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) const override;
};

}  // namespace qgbemu

