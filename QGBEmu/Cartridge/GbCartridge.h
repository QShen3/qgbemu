/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <fstream>
#include <memory>
#include <string>
#include <vector>

#include "QGBEmu/Cartridge/GbCartridgeInterface.h"
#include "QGBEmu/Cartridge/GbMbcFactoryInterface.h"
#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"

namespace qgbemu
{

class GbCartridge : public GbCartridgeInterface
{
    public:
        GbCartridge() = delete;
        explicit GbCartridge(const GbMbcFactoryInterface& gbMbcFactory);
        ~GbCartridge();

        void load(const char* filePath) override;

        uint8_t readFromRom(size_t address) override;
        uint8_t readFromRam(size_t address) override;
        void writeToRam(size_t address, uint8_t value) override;

        bool isGbcMode() const;

    private:
        bool validate(const std::vector<uint8_t>& buffer) const;
        size_t getRomSizeFromCartridge(uint8_t flag) const;
        size_t getRamSizeFromCartridge(uint8_t flag) const;
        std::string getPublisherFromCartridge(const std::vector<uint8_t>& buffer) const;

        std::ifstream file;
        const GbMbcFactoryInterface& gbMbcFactory;
        std::unique_ptr<GbMbcInterface> mbc;

        bool isLoaded;

        size_t romSize;
        size_t ramSize;
        std::string title;
        std::string publisher;
        bool gbcMode;
        uint8_t type;
        bool isJapan;
};

}  // namespace qgbemu
