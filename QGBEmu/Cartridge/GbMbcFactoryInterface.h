/* 
 *  Copyright [2021] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <memory>
#include <vector>

#include "QGBEmu/Cartridge/Mbc/GbMbcInterface.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemu
{

class GbCartridge;

class GbMbcFactoryInterface
{
    public:
        virtual ~GbMbcFactoryInterface() = default;

        virtual std::unique_ptr<GbMbcInterface> create(std::vector<Rom<uint8_t, romBankSize>>&& romBanks, std::vector<Ram<uint8_t, ramBankSize>>&& ramBanks, uint8_t type) const = 0;
};

}  // namespace qgbemu
