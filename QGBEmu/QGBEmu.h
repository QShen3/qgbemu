/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <array>
#include <cstddef>
#include <memory>

#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtQml/QQmlApplicationEngine>

#include "QGBEmu/Cartridge/GbCartridge.h"
#include "QGBEmu/Cartridge/GbMbcFactory.h"
#include "QGBEmu/Cpu/GbCpu.h"
#include "QGBEmu/Gpu/GbGpu.h"
#include "QGBEmu/Memory/GbMemoryMapper.h"
#include "QGBEmu/Memory/Ram.h"

namespace qgbemu
{

class QGBEmu : public QObject
{
    Q_OBJECT
    public:
        QGBEmu() = delete;
        explicit QGBEmu(const QQmlApplicationEngine& engine);
        ~QGBEmu() = default;

    public slots:
        void loadCartridge(const char* filePath);
        void loadCartridge(const QUrl& filePath);

    private:
        const GbMbcFactory gbMbcFactory;
        std::unique_ptr<GbCpu> cpu;
};

}  // namespace qgbemu

