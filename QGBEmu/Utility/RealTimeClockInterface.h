/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <functional>

namespace qgbemu
{

template<typename T = std::function<void()>>
class RealTimeClockInterface
{
    public:
        virtual ~RealTimeClockInterface() = default;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void setFunction(T function) = 0;
        virtual void clearFunction() = 0;

        virtual uint8_t getSeconds() const = 0;
        virtual void setSeconds(uint8_t value) = 0;

        virtual uint8_t getMinutes() const = 0;
        virtual void setMinutes(uint8_t value) = 0;

        virtual uint8_t getHours() const = 0;
        virtual void setHours(uint8_t value) = 0;

        virtual uint16_t getDays() const = 0;
        virtual void setDays(uint16_t value) = 0;

        virtual bool getCarry() const = 0;
        virtual void setCarry(bool value) = 0;
};

}  // namespace qgbemu
