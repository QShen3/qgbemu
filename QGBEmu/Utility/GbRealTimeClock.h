/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <cstdint>
#include <ctime>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <utility>

#include <QtCore/QTimer>

#include "QGBEmu/Utility/RealTimeClockInterface.h"

namespace qgbemu
{

template<typename T = std::function<void()>, typename Timer = QTimer>
class GbRealTimeClock : public RealTimeClockInterface<T>
{
    public:
        explicit GbRealTimeClock(std::unique_ptr<Timer> timer);
        GbRealTimeClock(std::unique_ptr<Timer> timer, const std::tm& timeData, bool carry, T function);

        void start() override;
        void stop() override;
        void setFunction(T function) override;
        void clearFunction() override;

        uint8_t getSeconds() const override;
        void setSeconds(uint8_t value) override;
        uint8_t getMinutes() const override;
        void setMinutes(uint8_t value) override;
        uint8_t getHours() const override;
        void setHours(uint8_t value) override;
        uint16_t getDays() const override;
        void setDays(uint16_t value) override;
        bool getCarry() const override;
        void setCarry(bool value) override;

    private:
        std::unique_ptr<Timer> timer;
        QMetaObject::Connection connection;
        mutable std::shared_mutex mutex;

        std::tm timeData;
        bool carry;
};

template<typename T, typename Timer>
GbRealTimeClock<T, Timer>::GbRealTimeClock(std::unique_ptr<Timer> timer) : timer(std::move(timer)), timeData({}), carry(false)
{
    QObject::connect(this->timer.get(), &Timer::timeout, [this](){
        std::unique_lock lock(mutex);
        timeData.tm_sec++;
        if (timeData.tm_sec == 60) {
            timeData.tm_sec = 0;
            timeData.tm_min++;
            if (timeData.tm_min == 60) {
                timeData.tm_min = 0;
                timeData.tm_hour++;
                if (timeData.tm_hour == 24) {
                    timeData.tm_hour = 0;
                    timeData.tm_yday++;
                    if (timeData.tm_yday == 512) {
                        timeData.tm_yday = 0;
                        carry = true;
                    }
                }
            }
        }
    });
    this->timer->setInterval(1000);
    this->timer->start();
}

template<typename T, typename Timer>
GbRealTimeClock<T, Timer>::GbRealTimeClock(std::unique_ptr<Timer> timer, const std::tm& timeData, bool carry, T function) : GbRealTimeClock(std::move(timer))
{
    connection = QObject::connect(this->timer.get(), &Timer::timeout, function);
    std::unique_lock lock(mutex);
    this->timeData = timeData;
    this->carry = carry;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::start()
{
    std::unique_lock lock(mutex);
    timer->start();
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::stop()
{
    std::unique_lock lock(mutex);
    timer->stop();
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setFunction(T function)
{
    clearFunction();
    std::unique_lock lock(mutex);
    connection = QObject::connect(timer.get(), &Timer::timeout, function);
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::clearFunction()
{
    std::shared_lock sharedLock(mutex);
    if (connection) {
        sharedLock.unlock();
        std::unique_lock uniqueLock(mutex);
        QObject::disconnect(connection);
    }
}

template<typename T, typename Timer>
uint8_t GbRealTimeClock<T, Timer>::getSeconds() const
{
    std::shared_lock lock(mutex);
    return timeData.tm_sec;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setSeconds(uint8_t value)
{
    std::unique_lock lock(mutex);
    timeData.tm_sec = value;
}

template<typename T, typename Timer>
uint8_t GbRealTimeClock<T, Timer>::getMinutes() const
{
    std::shared_lock lock(mutex);
    return timeData.tm_min;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setMinutes(uint8_t value)
{
    std::unique_lock lock(mutex);
    timeData.tm_min = value;
}

template<typename T, typename Timer>
uint8_t GbRealTimeClock<T, Timer>::getHours() const
{
    std::shared_lock lock(mutex);
    return timeData.tm_hour;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setHours(uint8_t value)
{
    std::unique_lock lock(mutex);
    timeData.tm_hour = value;
}

template<typename T, typename Timer>
uint16_t GbRealTimeClock<T, Timer>::getDays() const
{
    std::shared_lock lock(mutex);
    return timeData.tm_yday;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setDays(uint16_t value)
{
    std::unique_lock lock(mutex);
    timeData.tm_yday = value;
}

template<typename T, typename Timer>
bool GbRealTimeClock<T, Timer>::getCarry() const
{
    std::shared_lock lock(mutex);
    return carry;
}

template<typename T, typename Timer>
void GbRealTimeClock<T, Timer>::setCarry(bool value)
{
    std::unique_lock lock(mutex);
    carry = value;
}

}  // namespace qgbemu
