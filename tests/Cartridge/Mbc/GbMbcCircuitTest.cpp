/* 
 *  Copyright [2021] <qazxdrcssc2006@163.com>
 */

#include <array>
#include <cstddef>
#include <cstdint>
#include <random>
#include <vector>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable:4275 )
#endif
#include <gtest/gtest.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "QGBEmu/Cartridge/Mbc/GbMbcCircuit.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemuTest
{

class GbMbcCircuitTest : public testing::Test
{
    public:
        GbMbcCircuitTest() {}
};

TEST_F(GbMbcCircuitTest, ReadFromRom)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 255);

    std::array<uint8_t, qgbemu::romBankSize> data0;
    std::array<uint8_t, qgbemu::romBankSize> data1;

    for (auto& data : data0) {
        data = distrib(gen);
    }

    for (auto& data : data1) {
        data = distrib(gen);
    }

    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(2);
    romBanks.at(0).load(data0);
    romBanks.at(1).load(data1);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);

    qgbemu::GbMbcCircuit gbMbcCircuit(std::move(romBanks), std::move(ramBanks), 0);

    for (size_t i = 0; i < qgbemu::romBankSize; i++) {
        EXPECT_EQ(gbMbcCircuit.readFromRom(i), data0.at(i));
        EXPECT_EQ(gbMbcCircuit.readFromRom(i + qgbemu::romBankSize), data1.at(i));
    }
}

#ifdef NDEBUG
TEST_F(GbMbcCircuitTest, ReadFromNoneExistRam)
{
    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(2);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);

    qgbemu::GbMbcCircuit gbMbcCircuit(std::move(romBanks), std::move(ramBanks), 0);

    for (size_t i = 0; i < qgbemu::ramBankSize; i++) {
        EXPECT_EQ(0xFF, gbMbcCircuit.readFromRam(i));
    }
}
#endif

TEST_F(GbMbcCircuitTest, ReadFromRam)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 255);

    std::array<uint8_t, qgbemu::ramBankSize> data0;

    for (auto& data : data0) {
        data = distrib(gen);
    }

    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(2);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);
    ramBanks.at(0).load(data0);

    qgbemu::GbMbcCircuit gbMbcCircuit(std::move(romBanks), std::move(ramBanks), 1);

    for (size_t i = 0; i < qgbemu::ramBankSize; i++) {
        EXPECT_EQ(gbMbcCircuit.readFromRam(i), data0.at(i));
    }
}

TEST_F(GbMbcCircuitTest, WriteToRam)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 255);

    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(2);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);
    qgbemu::GbMbcCircuit gbMbcCircuit(std::move(romBanks), std::move(ramBanks), 1);

    for (size_t i = 0; i < qgbemu::ramBankSize; i++) {
        uint8_t value = distrib(gen);
        gbMbcCircuit.writeToRam(i, value);
        EXPECT_EQ(gbMbcCircuit.readFromRam(i), value);
    }
}

}  // namespace qgbemuTest
