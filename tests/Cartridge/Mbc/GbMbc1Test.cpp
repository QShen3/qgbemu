/* 
 *  Copyright [2021] <qazxdrcssc2006@163.com>
 */

#include <array>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <random>
#include <vector>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable:4275 )
#endif
#include <gtest/gtest.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "QGBEmu/Cartridge/Mbc/GbMbc1.h"
#include "QGBEmu/Memory/Ram.h"
#include "QGBEmu/Memory/Rom.h"

namespace qgbemuTest
{

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> distrib;

class GbMbc1Test : public testing::Test
{
    public:
        GbMbc1Test() {}
};

TEST_F(GbMbc1Test, SetControlRegister)
{
    size_t address;
    uint8_t value;

    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(1);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);
    qgbemu::GbMbc1 gbMbc1(std::move(romBanks), std::move(ramBanks), 0);

    distrib.param(std::uniform_int_distribution<>::param_type(0, 0x1FFF));
    address = distrib(gen);
    gbMbc1.setControlRegister(address, 1);
    EXPECT_EQ(gbMbc1.registers.ramEnable, 1);

    address = distrib(gen);
    gbMbc1.setControlRegister(address, 0);
    EXPECT_EQ(gbMbc1.registers.ramEnable, 0);

    distrib.param(std::uniform_int_distribution<>::param_type(0x2000, 0x3FFF));
    address = distrib(gen);
    distrib.param(std::uniform_int_distribution<>::param_type(0, 64));
    value = distrib(gen);
    gbMbc1.setControlRegister(address, value);
    EXPECT_EQ(gbMbc1.registers.romBankNumber, value);

    distrib.param(std::uniform_int_distribution<>::param_type(0x4000, 0x5FFF));
    address = distrib(gen);
    distrib.param(std::uniform_int_distribution<>::param_type(0, 64));
    value = distrib(gen);
    gbMbc1.setControlRegister(address, value);
    EXPECT_EQ(gbMbc1.registers.ramBankNumber, value & 0b11);

    distrib.param(std::uniform_int_distribution<>::param_type(0x6000, 0x7FFF));
    address = distrib(gen);
    distrib.param(std::uniform_int_distribution<>::param_type(0, 64));
    value = distrib(gen);
    gbMbc1.setControlRegister(address, value);
    EXPECT_EQ(gbMbc1.registers.romRamModeSelect, value & 0b1);
}

TEST_F(GbMbc1Test, ReadFrom1BankRom)
{
    distrib.param(std::uniform_int_distribution<>::param_type(0, 255));

    auto romDataBanks = std::make_unique<std::array<std::array<uint8_t, qgbemu::romBankSize>, 1>>();
    for (auto& romData : *romDataBanks) {
        for (auto& data : romData) {
            data = distrib(gen);
        }
    }

    std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(1);
    std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);
    for (size_t i = 0; i < romBanks.size(); i++) {
        romBanks.at(i).load(romDataBanks->at(i));
    }

    qgbemu::GbMbc1 gbMbc1(std::move(romBanks), std::move(ramBanks), 0);
    gbMbc1.registers.romRamModeSelect = 0;
    for (size_t i = 0; i < qgbemu::romBankSize; i++) {
        EXPECT_EQ(gbMbc1.readFromRom(i), romDataBanks->at(0).at(i));
    }
    gbMbc1.registers.romRamModeSelect = 1;
    gbMbc1.registers.ramBankNumber = 1;
    EXPECT_THROW(gbMbc1.readFromRom(1), std::out_of_range);
    EXPECT_THROW({
        try {
            gbMbc1.readFromRom(0x4000);
        }
        catch(const std::runtime_error& e) {
            EXPECT_STREQ(e.what(), "Invalid address!");
            throw;
        }
    }, std::runtime_error);
}

TEST_F(GbMbc1Test, ReadFromRom)
{
    distrib.param(std::uniform_int_distribution<>::param_type(0, 255));

    auto romDataBanks = std::make_unique<std::array<std::array<uint8_t, qgbemu::romBankSize>, 128>>();
    for (auto& romData : *romDataBanks) {
        for (auto& data : romData) {
            data = distrib(gen);
        }
    }

    for (size_t size = 2; size <= 128; size = size * 2) {
        std::vector<qgbemu::Rom<uint8_t, qgbemu::romBankSize>> romBanks(size);
        std::vector<qgbemu::Ram<uint8_t, qgbemu::ramBankSize>> ramBanks(1);
        for (size_t i = 0; i < romBanks.size(); i++) {
            romBanks.at(i).load(romDataBanks->at(i));
        }

        qgbemu::GbMbc1 gbMbc1(std::move(romBanks), std::move(ramBanks), 0);
        gbMbc1.registers.romRamModeSelect = 0;
        for (size_t i = 0; i < qgbemu::romBankSize; i++) {
            EXPECT_EQ(gbMbc1.readFromRom(i), romDataBanks->at(0).at(i));
        }
        gbMbc1.registers.romRamModeSelect = 1;
        for (size_t i = 0x20; i < size; i = i + 0x20) {
            gbMbc1.registers.ramBankNumber = uint8_t(i >> 5);
            for (size_t j = 0; j < qgbemu::romBankSize; j++) {
                EXPECT_EQ(gbMbc1.readFromRom(j), romDataBanks->at(i).at(j));
            }
        }

        for (size_t i = qgbemu::romBankSize; i < qgbemu::romBankSize * 2; i++) {
            for (uint8_t j = 1; j < (size & 32); j++) {
                if (size < 64) {
                    gbMbc1.registers.romBankNumber = (j & 0b11111);
                    EXPECT_EQ(gbMbc1.readFromRom(i), romDataBanks->at(j).at(i & 0xBFFF));
                }
                else {
                    gbMbc1.registers.romBankNumber = (j & 0b11111);
                    for (uint8_t k = 1; k < (size >> 5); k++) {
                        gbMbc1.registers.ramBankNumber = k;
                        EXPECT_EQ(gbMbc1.readFromRom(i), romDataBanks->at((k << 5) | j).at(i & 0xBFFF));
                    }
                }
            }
        }
    }
}

}  // namespace qgbemuTest
