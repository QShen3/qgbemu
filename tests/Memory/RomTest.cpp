/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cstddef>
#include <cstdint>
#include <stdexcept>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable:4275 )
#endif
#include <gtest/gtest.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "QGBEmu/Memory/Rom.h"

namespace qgbemuTest
{

constexpr size_t romSize = 4;

class RomTest : public ::testing::Test
{
};

TEST_F(RomTest, Constructor)
{
    qgbemu::Rom<uint8_t, romSize> rom;
    for (size_t i = 0; i < rom.size(); i++) {
        EXPECT_EQ(rom.read(i), 0);
    }
}

TEST_F(RomTest, Write)
{
    qgbemu::Rom<uint8_t, romSize> rom;
    EXPECT_THROW({
            try {
                rom.write(0, 1);
            }
            catch(const std::runtime_error& e) {
                EXPECT_STREQ(e.what(), "This memory is readonly!");
                throw;
            }
        }, std::runtime_error);
}

TEST_F(RomTest, Size)
{
    qgbemu::Rom<uint8_t, romSize> rom;
    EXPECT_EQ(rom.size(), romSize);
}

TEST_F(RomTest, Load)
{
    qgbemu::Rom<uint8_t, romSize> rom;
    rom.load({1, 2, 3, 4});
    EXPECT_EQ(rom.read(0), 1);
    EXPECT_EQ(rom.read(1), 2);
    EXPECT_EQ(rom.read(2), 3);
    EXPECT_EQ(rom.read(3), 4);
}

}  // namespace qgbemuTest
