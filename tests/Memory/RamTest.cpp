/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <random>
#include <stdexcept>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable:4275 )
#endif
#include <gtest/gtest.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "QGBEmu/Memory/Ram.h"

namespace qgbemuTest
{

constexpr size_t ramSize = 4;

class RamTest : public ::testing::Test
{
};

TEST_F(RamTest, Constructor)
{
    qgbemu::Ram<uint8_t, ramSize> ram;
    for (size_t i = 0; i < ram.size(); i++) {
        EXPECT_EQ(ram.read(i), 0);
    }
}

TEST_F(RamTest, ReadAndWrite)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, INT_MAX);
    qgbemu::Ram<int, ramSize> ram;
    for (size_t i = 0; i < ram.size(); i++) {
        auto number = distrib(gen);
        ram.write(i, number);
        EXPECT_EQ(ram.read(i), number);
    }
}

TEST_F(RamTest, Size)
{
    qgbemu::Ram<uint8_t, ramSize> ram;
    EXPECT_EQ(ram.size(), ramSize);
}

TEST_F(RamTest, Load)
{
    qgbemu::Ram<uint8_t, ramSize> ram;
    ram.load({1, 2, 3, 4});
    EXPECT_EQ(ram.read(0), 1);
    EXPECT_EQ(ram.read(1), 2);
    EXPECT_EQ(ram.read(2), 3);
    EXPECT_EQ(ram.read(3), 4);
}

}  // namespace qgbemuTest
