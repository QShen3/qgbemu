/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#include <memory>

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable:4275 )
#endif
#include <gtest/gtest.h>
#ifdef _MSC_VER
#pragma warning( pop )
#endif

#include "QGBEmu/Utility/GbRealTimeClock.h"
#include "mock/Utility/MockQTimer.h"

namespace qgbemuTest
{

class GbRealTimeClockTest : public ::testing::Test
{
    public:
        GbRealTimeClockTest() : timer(std::make_unique<testing::StrictMock<qgbemuMock::MockQTimer>>()) {}

    protected:
        std::unique_ptr<testing::StrictMock<qgbemuMock::MockQTimer>> timer;
};

TEST_F(GbRealTimeClockTest, ConstructorWithDefaultParameters)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    EXPECT_EQ(gbRealTimeClock.getSeconds(), 0);
    EXPECT_EQ(gbRealTimeClock.getMinutes(), 0);
    EXPECT_EQ(gbRealTimeClock.getHours(), 0);
    EXPECT_EQ(gbRealTimeClock.getDays(), 0);
    EXPECT_EQ(gbRealTimeClock.getCarry(), false);
}

TEST_F(GbRealTimeClockTest, ConstructorWithTimeDataAndFunction)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    std::tm timeData {};
    timeData.tm_sec = 1;
    timeData.tm_min = 1;
    timeData.tm_hour = 1;
    timeData.tm_yday = 1;
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer), timeData, true, [](){});

    EXPECT_EQ(gbRealTimeClock.getSeconds(), 1);
    EXPECT_EQ(gbRealTimeClock.getMinutes(), 1);
    EXPECT_EQ(gbRealTimeClock.getHours(), 1);
    EXPECT_EQ(gbRealTimeClock.getDays(), 1);
    EXPECT_EQ(gbRealTimeClock.getCarry(), true);
}

TEST_F(GbRealTimeClockTest, StartAndStop)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(2);
    EXPECT_CALL(*timer, stop()).Times(1);

    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));
    gbRealTimeClock.stop();
    gbRealTimeClock.start();
}

TEST_F(GbRealTimeClockTest, GetAndSetSeconds)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    gbRealTimeClock.setSeconds(10);
    EXPECT_EQ(10, gbRealTimeClock.getSeconds());
    gbRealTimeClock.setSeconds(0);
    EXPECT_EQ(0, gbRealTimeClock.getSeconds());
}

TEST_F(GbRealTimeClockTest, GetAndSetMinutes)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    gbRealTimeClock.setMinutes(10);
    EXPECT_EQ(10, gbRealTimeClock.getMinutes());
    gbRealTimeClock.setMinutes(0);
    EXPECT_EQ(0, gbRealTimeClock.getMinutes());
}

TEST_F(GbRealTimeClockTest, GetAndSetHours)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    gbRealTimeClock.setHours(10);
    EXPECT_EQ(10, gbRealTimeClock.getHours());
    gbRealTimeClock.setHours(0);
    EXPECT_EQ(0, gbRealTimeClock.getHours());
}

TEST_F(GbRealTimeClockTest, GetAndSetDays)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    gbRealTimeClock.setDays(10);
    EXPECT_EQ(10, gbRealTimeClock.getDays());
    gbRealTimeClock.setDays(0);
    EXPECT_EQ(0, gbRealTimeClock.getDays());
}

TEST_F(GbRealTimeClockTest, GetAndSetCarry)
{
    EXPECT_CALL(*timer, setInterval(1000)).Times(1);
    EXPECT_CALL(*timer, start()).Times(1);
    qgbemu::GbRealTimeClock<std::function<void()>, qgbemuMock::MockQTimer> gbRealTimeClock(std::move(timer));

    gbRealTimeClock.setCarry(true);
    EXPECT_EQ(true, gbRealTimeClock.getCarry());
    gbRealTimeClock.setCarry(false);
    EXPECT_EQ(false, gbRealTimeClock.getCarry());
}

}  // namespace qgbemuTest
