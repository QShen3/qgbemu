/* 
 *  Copyright [2020] <qazxdrcssc2006@163.com>
 */

#pragma once

#include <gmock/gmock.h>
#include <QtCore/QTimer>

namespace qgbemuMock
{

class MockQTimer : public QTimer
{
    public:
#if defined(__GNUC__) && __GNUC__ < 9
        MOCK_METHOD1(setInterval, void(int msec));
        MOCK_METHOD0(start, void());
        MOCK_METHOD0(stop, void());
#else
        MOCK_METHOD(void, setInterval, (int msec), ());
        MOCK_METHOD(void, start, (), ());
        MOCK_METHOD(void, stop, (), ());
#endif
};

}  // namespace qgbemuMock
