# BUILD

## Windows

### MSVC

#### Prerequisite

Recommand using Chocolatey to install prerequisite
```cmd
choco install visualstudio2019buildtools cmake ninja
```

#### Build
```cmd
"C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"

mkdir buid && cd build
cmake -G Ninja
ninja

ninja test
```

### MinGW GCC

#### Prerequisite

```cmd
choco install msys2
C:\tools\msys64\msys2_shell.cmd -defterm -no-start -ucrt64
```

```shell
pacman -S mingw-w64-ucrt-x86_64-toolchain mingw-w64-ucrt-x86_64-cmake mingw-w64-ucrt-x86_64-ninja
pacman -S mingw-w64-ucrt-x86_64-qt6-declarative mingw-w64-ucrt-x86_64-opencv mingw-w64-ucrt-x86_64-gtest
```

#### Build

```
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### MinGW Clang

#### Prerequisite

```cmd
choco install msys2
C:\tools\msys64\msys2_shell.cmd -defterm -no-start -clang64
```

```shell
pacman -S mingw-w64-clang-x86_64-toolchain mingw-w64-clang-x86_64-cmake mingw-w64-clang-x86_64-ninja
pacman -S mingw-w64-clang-x86_64-qt6-declarative mingw-w64-clang-x86_64-opencv mingw-w64-clang-x86_64-gtest
```

#### Build

```
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

## Linux

### Alpine

#### Prerequisite

```shell
apk add gcc g++ cmake samurai
apk add qt6-qtdeclarative-dev opencv-dev gtest-dev
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### Arch

#### Prerequisite

```shell
pacman -S gcc cmake ninja
pacman -S qt6-declarative opencv gtest
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### Fedora/CentOS/Rocky

#### Prerequisite

```shell
# Note: For CentOS and Rocky Linux, you need to enable powertoys and raven repo first
dnf install gcc gcc-c++ cmake ninja-build
dnf install qt6-qtdeclarative-devel opencv-devel gtest-devel gmock-devel
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### Debian/Ubuntu

#### Prerequisite

```shell
apt-get install gcc g++ cmake ninja-build
apt-get install qt6-declarative-dev libopencv-dev libgtest-dev libgmock-dev
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### VoidLinux

#### Prerequisite
```shell
xbps-install gcc cmake ninja
xbps-install qt6-declarative-devel libopencv-devel gtest-devel
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

### Gentoo

#### Prerequisite

```shell
emerge cmake ninja
emerge qtquickcontrols2 opencv gtest
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja -DQGBEMU_QT_MAJOR_VERSION=5 -DCMAKE_CXX_FLAGS=-pthread
ninja

ninja test
```

## MacOS

### Apple-Clang

#### Prerequisite

```shell
brew install cmake ninja
brew install qt opencv googletest
```

#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja
ninja

ninja test
```

## BSD

### FreeBSD

#### Prerequisite

```shell
pkg install cmake ninja
pkg install qt5-buildtools qt5-quickcontrols2 opencv googletest
```
#### Build

```shell
mkdir build && cd build
cmake .. -G Ninja -DQGBEMU_QT_MAJOR_VERSION=5 -DCMAKE_CXX_FLAGS=-pthread
ninja

ninja test
```



